	// start module.exports = function(grunt)
module.exports = function(grunt) {
	// start grunt.initConfig
	grunt.initConfig({
// watch
		watch: {
			scripts: {
				files: ['sass/**/*.scss'],
				tasks: ['sass']
			}
		},
// sass

		sass: {
			dist: {
				options: {
					style: 'expanded'
				},
					files: {
'css/style.css': 'sass/*.scss'
					}
			}
		}

	});
       	// end = grunt.config.init

	// start grunt.loadNpmTasks
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	// end grunt.loadNpmTasks
	//
	// start grunt.registerTask
	grunt.registerTask('default', ['watch']); // string for tasks
	// end grunt.registerTask
}; // end  = module.exports = function(grunt)


